use chrono::prelude::*;
use serde::{de, Deserialize, Deserializer,Serialize};
use std::cmp::{Eq, Ord, Ordering, PartialOrd};

pub mod site;

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub enum EventType { CornerKick, Foul, Fulltime, FreeKick, Explusion, Goal, Halftime, Kickoff, Penalty, RedCard, YellowCard }
#[derive(Clone, Deserialize, Debug, Eq, PartialEq, Hash, Serialize)]
pub enum Position {Forward, Defender, Goalkeeper,
    #[serde(alias = "Goalkeeper Coach")]
    GoalkeeperCoach,
    #[serde(alias = "Head Coach")]
    HeadCoach,
    Midfielder,
    #[serde(alias = "Technical Director")]
    TechnicalDirector }

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Attendence { pub count: u32, pub source: String }
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Broadcast { pub commentators: String, pub video_link: String }
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Club {
    pub full_name: String,
    pub trivial_name: String,
    pub short: String,
    pub colors: Option<Colors>,
    pub website: Option<String>,
    pub social: Option<SocialLinks>
}
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Colors { pub primary: String, pub secondary: String, pub tertiary: String }
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Event { pub minute: u8, pub home_score: u8, pub away_score: u8, event_type: EventType, descr: String }
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Info {
    pub name: String,
    pub description: String,
}
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct NISA { pub seasons: Vec<Season>, pub clubs: Vec<Club> }

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Biography {
    pub de: Option<String>,
    pub en: Option<String>,
    pub es: Option<String>
}

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Player {
    pub name: String,
    #[serde(alias = "squadNumber")]
    pub squad_number: String,
    pub position: Position,
    pub thumbnail: Option<String>,
    pub bio: Biography
   
    /*
   
     #[serde(alias = "team")]
    pub club: String, //
    pub images: Vec<String>, //
    pub twitter: String,
    pub instagram: String,
    // #[serde(deserialize_with = "deserialize_from_str")]
    // #[serde(alias = "createdAt")]
    // pub created: DateTime<FixedOffset>,
    // #[serde(deserialize_with = "deserialize_from_str")]
    // #[serde(alias = "updatedAt")]
    // pub updated: DateTime<FixedOffset>,
    // "update_time": "2020-03-24T10:25:44.702Z",
    pub flag: u8
    */
}

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Match { 
    //TODO: home: Club, away: Club 
    pub home_short: String,
    pub away_short: String,
    #[serde(deserialize_with = "deserialize_from_str")]
    pub date: DateTime<FixedOffset>,
    pub attendence: Option<Attendence>,
    pub broadcasts: Option<Vec<Broadcast>>,
    pub result: Option<Vec<u8>>,
    pub info: Option<Info>,
    pub events: Option<Vec<Event>>
}

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct Season {
    pub id: u8,
    pub info: Info,
    pub matches: Vec<Match>
}

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub enum Scope { All, Previous, Upcoming }

#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct SocialLinks {
    facebook: Option<String>,
    instagram: Option<String>,
    twitter: Option<String>
}

impl PartialEq for Club {
    fn eq(&self, other: &Self) -> bool {
        self.short == other.short
    }
}
impl Eq for Club {}

impl Eq for Match {}

impl Ord for Match {
    fn cmp(&self, other: &Self) -> Ordering {
        self.date.cmp(&other.date)
    }
}

impl PartialEq for Match {
    fn eq(&self, other: &Self) -> bool {
        self.home_short == other.away_short &&
        self.away_short == other.away_short &&
        self.date == other.date
    }
}

impl PartialOrd for Match {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn deserialize_from_str<'de, D>(deserializer: D) -> Result<DateTime<FixedOffset>, D::Error>
    where D: Deserializer<'de>, {
        let date: String = Deserialize::deserialize(deserializer)?;
        date.parse::<DateTime<FixedOffset>>().map_err(de::Error::custom)
}

pub trait Create { fn create(&self); }
pub trait Read{ fn read() -> std::io::Result<Self> where Self: std::marker::Sized; }
pub trait Update<T> { fn update(&self, data: T) -> std::io::Result<()>; }
pub trait Delete { fn delete(&self); }
