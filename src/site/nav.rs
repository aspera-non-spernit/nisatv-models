use serde::{Deserialize, Serialize};
use std::io::Read;
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct IconLink {
    title: String,
    href: String,
    #[serde(alias = "a-short")]
    a_short: String
}
#[derive(Clone, Deserialize, Debug, Hash, Serialize)]
pub struct IconMenu {
    icon_links: Vec<IconLink>
}

impl crate::Read for IconMenu {
    fn read() -> Result<Self, std::io::Error> {
        let mut file = std::fs::File::open("config/main_menu.json")?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        let icon_menu: Self = serde_json::from_str(&contents).unwrap();
        Ok(icon_menu)
    }
}