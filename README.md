# nisatv-models

A library of models and implementations for a Soccer / Sports environment,
written in rust for the nisatv-rest service and the nisatv-webapp.

# Usage

Add to your Cargo.toml:

```toml
nisatv-models = {git="https://gitlab.com/aspera-non-spernit/nisatv-models", branch="dev" }
```

Add to your project :

```rust
extern crate nisatv_models;
```
